import React from 'react'
import { Helmet } from 'react-helmet'
import Landing from '../../components/container/landing'
import Navbar from '../../components/container/navbar'
import Projects from '../../components/container/projects'
import About from '../../components/core-ui/about/about'
import { headerData } from '../../data/headerData'
import { TranslationProvider } from '../../TranslationContext'
import LanguageSwitcher from '../../LanguageSwitcher'

function HomePage() {
    return (
        <div>
            <TranslationProvider>
            <Helmet>
            <title>{headerData.name}Special links page</title>
            </Helmet>
            <LanguageSwitcher />
            <Navbar />
            <Landing />
            <Projects />
            <About />
            </TranslationProvider>
        </div>
    )
}

export default HomePage
