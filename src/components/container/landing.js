import { makeStyles } from '@mui/styles';
import React, { useContext } from 'react';
import { ThemeContext } from '../../contexts/theme-context';
import LandingUI from '../core-ui/landing/landing-ui';

const Landing = () => {
  const { theme, drawerOpen } = useContext(ThemeContext);

  const useStyles = makeStyles((t) => ({
    contactBtn: {
      backgroundColor: theme.primary,
      color: theme.secondary,
      marginTop: '20px',
      borderRadius: '30px',
      textTransform: 'inherit',
      textDecoration: 'none',
      width: '150px',
      height: '50px',
      fontSize: '1.5rem',
      boxShadow: '0px 0px 12px 2px #f75cee',
      textShadow: '0 1px 1px #4f555c, 0 0 0.1em #818994, 0 0 0.1em #0a0a0a',
      fontWeight: '500',
      cursor: 'pointer',
      fontFamily: 'var(--primaryFont)',
      border: `3px solid ${theme.primary}`,
      transition: '500ms ease-out',
      '&:hover': {
        transform: 'scale(1.1)',
        backgroundColor: theme.primary,
        color: theme.secondary,
        border: `3px solid white`,
      },
      [t.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
  }));

  const classes = useStyles();

  return (
    <LandingUI drawerOpen={drawerOpen} theme={theme} classes={classes} />
  );
};

export default Landing;
