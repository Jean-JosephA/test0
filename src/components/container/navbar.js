import { makeStyles } from '@mui/styles';
import React, { useContext, useState } from 'react';
import { ThemeContext } from '../../contexts/theme-context';
import NavbarUI from '../core-ui/navbar/navbar-ui';

const Navbar = () => {
  const { theme, setHandleDrawer, changeTheme, isDark } = useContext(ThemeContext);
  const [open, setOpen] = useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
    setHandleDrawer();
  };

  const handleDrawerClose = () => {
    setOpen(false);
    setHandleDrawer();
  };

  const useStyles = makeStyles((t) => ({
    navMenu: {
      fontSize: '2.2rem',
      color: theme.tertiary,
      cursor: 'pointer',
      transform: 'translateY(-10px)',
      transition: 'color 0.3s',
      '&:hover': {
        color: theme.primary,
      },
      [t.breakpoints.down('sm')]: {
        fontSize: '2.5rem',
      },
      [t.breakpoints.down('xs')]: {
        fontSize: '2rem',
      },
    },    
    MuiDrawer: {
      padding: '0em 1.5em',
      width: '14em',
      fontFamily: ' var(--primaryFont)',
      fontStyle: ' normal',
      fontWeight: ' normal',
      fontSize: ' 25px',
      background: theme.secondary,
      overflow: 'hidden',
      borderTopRightRadius: '40px',
      borderBottomRightRadius: '40px',
      textShadow: '0 1px 1px #fcfeff,0 0 0.1em #818994,0 0 .1em #0a0a0a',
      [t.breakpoints.down('sm')]: {
        width: '12em',
      },
    },
    closebtnIcon: {
      fontSize: '2.1rem',
      fontWeight: 'bold',
      cursor: 'pointer',
      color: theme.primary,
      position: 'absolute',
      right: 40,
      top: 40,
      boxShadow: '5px 5px 5px 1px rgba(1, 9, 20, .4)',
      transition: 'color 0.2s',
      '&:hover': {
        color: '#d4284d',
      },
      [t.breakpoints.down('sm')]: {
        right: 20,
        top: 20,
      },
    },
    drawerItem: {
      margin: '3rem auto',
      background: theme.secondary,
      color: theme.primary,
      width: '100%',
      height: '59px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      padding: '0px 25px',
      boxSizing: 'border-box',
      border: '3px solid',
      cursor: 'pointer',
      borderColor: theme.primary,
      boxShadow: '0 0 12px 2px #0d68c3',
      textShadow: '0 1px 1px #fcfeff,0 0 0.1em #818994,0 0 .1em #0a0a0a',
      borderRadius: '28px 28px 28px 28px',
      transition: 'background-color 0.2s, color 0.2s',
      '&:hover': {
        background: theme.primary,
        color: theme.secondary,
      },
      [t.breakpoints.down('sm')]: {
        width: '100%',
        padding: '0 25px',
        height: '55px',
      },
    },
    drawerLinks: {
      fontFamily: 'var(--primaryFont)',
      width: '50%',
      fontSize: '1.8rem',
      fontWeight: 400,
      alignItems: 'center',
      textShadow: '0 1px 1px #fcfeff,0 0 0.1em #818994,0 0 .1em #0a0a0a',
      cursor: 'pointer',
      inlineSize: 'auto',
      [t.breakpoints.down('sm')]: {
        fontSize: '1.125rem',
      },
    },
    drawerIcon: {
      fontSize: '1.6rem',
      [t.breakpoints.down('sm')]: {
        fontSize: '1.385rem',
        cursor: 'pointer',
      },
    },
  }));

  const classes = useStyles();

  const shortname = (name) => {
    if (name.length > 12) {
      return name.split(' ')[0];
    } else {
      return name;
    }
  };

  return (
    <NavbarUI
      theme={theme}
      shortname={shortname}
      classes={classes}
      handleDrawerOpen={handleDrawerOpen}
      handleDrawerClose={handleDrawerClose}
      open={open}
      changeTheme={changeTheme}
      isDark={isDark}
    />
  );
};

export default Navbar;
