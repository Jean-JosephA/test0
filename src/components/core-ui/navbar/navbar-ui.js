import { Drawer } from '@mui/material';
import React from 'react';
import { FaRegMoon , FaRegSun  } from 'react-icons/fa';
import { IoClose, IoMenuSharp } from 'react-icons/io5';
import Fade from 'react-reveal/Fade';
import { HashLink as NavLink } from 'react-router-hash-link';
import './navbar.css';
import { useTranslation } from '../../../TranslationContext';

function NavbarUI({ theme, classes, handleDrawerOpen, handleDrawerClose, open, changeTheme, isDark }) {
    const { translate } = useTranslation();

    return (
        <div className='navbar'>
            <div className='navbar--container'>
                <h1 style={{ color: theme.primary }}>
                {translate('title', 'navbar')}
                </h1>

                <IoMenuSharp
                    className={classes.navMenu}
                    onClick={handleDrawerOpen}
                    aria-label={translate('menu', 'navbar')}
                />
            </div>
            <Drawer
                variant='temporary'
                onClose={( reason ) => {
                    if (reason !== 'backdropClick') {
                        handleDrawerClose();
                    } else if (reason !== 'escapeKeyDown') {
                        handleDrawerClose();
                    }
                }}
                anchor='left'
                open={open}
                classes={{ paper: classes.MuiDrawer }}
                className='drawer'
                disableScrollLock={true}
            >
                <div className='div-closebtn'>
                    <IoClose
                        onClick={handleDrawerClose}
                        onKeyDown={(e) => {
                            if (e.key === ' ' || e.key === 'Enter') {
                                e.preventDefault();
                                handleDrawerClose();
                            }
                        }}
                        className={classes.closebtnIcon}
                        role='button'
                        tabIndex='0'
                        aria-label='Close'
                    />
                </div>
                <br />

                <div onClick={handleDrawerClose}>
                    <div className='navLink--container'>
                        <Fade left>
                            <NavLink
                                to='/'
                                smooth={true}
                                spy='true'
                                duration={2000}
                            >
                                <div className={classes.drawerItem}>
                                    
                                    <span className={classes.drawerLinks}>
                                        Home
                                    </span>
                                </div>
                            </NavLink>
                        </Fade>

                        <Fade left>
                            <NavLink
                                to='/#projects'
                                smooth={true}
                                spy='true'
                                duration={2000}
                            >
                                <div className={classes.drawerItem}>
                                    <span className={classes.drawerLinks}>
                                    {translate('My links', 'navbar')}
                                    </span>
                                </div>
                            </NavLink>
                        </Fade>

                        <Fade left>
                            <NavLink
                                to='/#about'
                                smooth={true}
                                spy='true'
                                duration={2000}
                            >
                                <div className={classes.drawerItem}>
                                    <span className={classes.drawerLinks}>
                                    {translate('about', 'navbar')}
                                    </span>
                                </div>
                            </NavLink>
                        </Fade>

                        <Fade left>
                            <div className={classes.drawerItem} onClick={changeTheme}>
                                {isDark ?
                                    <FaRegSun className={classes.drawerIcon} />
                                    :
                                    <FaRegMoon className={classes.drawerIcon} />
                                }
                                <span className={classes.drawerLinks}>
                                
                                        {translate('themeSwitch', 'navbar')}
                                
                                </span>
                            </div>
                        </Fade>
                    </div>
                </div>
            </Drawer>
        </div>
    );
}

export default NavbarUI;
