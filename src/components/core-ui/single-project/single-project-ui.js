import React from 'react';
import { FaMagic } from "react-icons/fa";
import placeholder from '../../../assets/png/placeholder.png';
import Fade from 'react-reveal/Fade';
import './single-project.css';

function SingleProjectUI({ id, name, desc, tags, code, demo, href, image, theme, classes }) {

    return (
        <Fade bottom>
            <div
                key={id}
                className='singleProject'
                style={{ backgroundColor: theme.quaternary }}
            >
                <div className='projectContent'>
                    <h2
                        id={name.replace(' ', '-').toLowerCase()}
                        style={{ color: theme.tertiary }}
                    >
                        {name}
                    </h2>
                    <a href={demo} target="_blank" rel="noopener noreferrer" >
                    <img src={image ? image : placeholder} alt={name} style={{ width: '100%', height: '100%' }}  /></a>
                    <div className='project--showcaseBtn'>
                        <a
                            href={demo}
                            target='_blank'
                            rel='noreferrer'
                            className={classes.iconBtn}
                            aria-labelledby={`${name
                                .replace(' ', '-')
                                .toLowerCase()} ${name
                                    .replace(' ', '-')
                                    .toLowerCase()}-demo`}
                        >
                           
                            <FaMagic
                                id={`${name
                                    .replace(' ', '-')
                                    .toLowerCase()}-code`}
                                className={classes.icon}
                                aria-label='Code'
                            />
                        </a>
                    </div>
                </div>
                {/* Removed the paragraph displaying the project description */}
                <div
                    className='project--lang'
                    style={{
                        background: theme.secondary,
                        color: theme.tertiary,
                    }}
                >
                    {tags.map((tag, id) => (
                        <span key={id}>{tag}</span>
                    ))}
                </div>
            </div>
        </Fade>
    );
}

export default SingleProjectUI;



